"""altium tap class."""

from __future__ import annotations

from singer_sdk import Tap
from singer_sdk import typing as th  # JSON schema typing helpers

# TODO: Import your custom stream types here:
from tap_altium import streams
import inspect

class Tapaltium(Tap):
    def __init__(
        self,
        config=None,
        catalog=None,
        state=None,
        parse_env_config=False,
        validate_config=True,
    ) -> None:
        self.config_file = config[0]
        super().__init__(config, catalog, state, parse_env_config, validate_config)

    """altium tap class."""
    
    name = "tap-altium"

    # TODO: Update this section with the actual config values you expect:
    config_jsonschema = th.PropertiesList(
        th.Property(
            "refresh_token",
            th.StringType,
            required=False, # Flag config as protected.
            description="The token to authenticate against the API service",
        ),
        th.Property(
            "client_id",
            th.StringType,
            required=True,
            description="Client ID for Nexar API"
        ),
        th.Property(
            "client_secret",
            th.StringType,
            required=True,
            description="Client Secret for Nexar API",
        ),
    ).to_dict()

    def discover_streams(self) -> list[streams.altiumStream]:
        """Return a list of discovered streams.

        Returns:
            A list of discovered streams.
        """
        return [
           cls(self) for name, cls in inspect.getmembers(streams,inspect.isclass) if cls.__module__ == 'tap_altium.streams'
        ]



if __name__ == "__main__":
    Tapaltium.cli()
