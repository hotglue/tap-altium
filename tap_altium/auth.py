"""altium Authentication."""

from __future__ import annotations

from singer_sdk.authenticators import OAuthAuthenticator, SingletonMeta
from datetime import datetime, timedelta
import requests
import json



# The SingletonMeta metaclass makes your streams reuse the same authenticator instance.
# If this behaviour interferes with your use-case, you can remove the metaclass.
class altiumAuthenticator(OAuthAuthenticator, metaclass=SingletonMeta):
    """Authenticator class for altium."""
    def __init__(self, stream,auth_endpoint,oauth_scopes):
        super().__init__(stream)
        self.stream = stream
        self._auth_endpoint = "https://identity.nexar.com/connect/token"
        self._oauth_scopes = "openid design.domain user.access"
        self._config = self.stream.config
        self._refresh_token = self._config["refresh_token"]
        self._access_token = self._config.get("access_token")
        self.expires_in = self.stream._tap._config.get("expires_in")

    @property
    def oauth_request_body(self) -> dict:
        """Define the OAuth request body for the AutomaticTestTap API.

        Returns:
            A dict with the request body
        """

        return { 
            "grant_type": "refresh_token",
            "scope": self._oauth_scopes,
            "client_id": self.config["client_id"],
            "client_secret":self.config["client_secret"],
            "refresh_token":self._refresh_token
        }
    
    def is_token_valid(self):
        now = round(datetime.utcnow().timestamp())
        day_hours = timedelta(hours=24)
        
        return not bool(
            (not self._access_token) or (not self.expires_in) or ((self.expires_in - now) < 60)
        )
    
    def update_access_token(self):
        request_time = datetime.utcnow()
        token_response = requests.post(
            self.auth_endpoint,
            data = self.oauth_request_body
        )
        request_time = round(datetime.utcnow().timestamp())
        try: 
            token_response.raise_for_status() 
            self.logger.info("OAuth authorization attempt was successful.")
        except Exception as ex:
            raise RuntimeError(f"Failed OAuth login, response was {token_response.json()} . {ex}")

        token_json = token_response.json()
        self._refresh_token = token_json["refresh_token"]
        self._access_token = token_json["access_token"]
        self.expires_in = request_time + token_json["expires_in"]

        self.stream._tap._config["access_token"] = self._access_token
        self.stream._tap._config["refresh_token"] = self._refresh_token
        self.stream._tap._config["expires_in"] = self.expires_in
        with open(self.stream._tap.config_file, "w") as outfile:
            json.dump(self.stream._tap._config, outfile, indent=4)


    @property
    def auth_headers(self):
        if self.is_token_valid():
            return {"token": self._access_token}

        else: 
            self.update_access_token()
            return {"token":self._access_token}


    @classmethod
    def create_for_stream(cls, stream) -> altiumAuthenticator:  # noqa: ANN001
        """Instantiate an authenticator for a specific Singer stream.

        Args:
            stream: The Singer stream instance.

        Returns:
            A new authenticator.
        """
        return cls(
            stream=stream,
            auth_endpoint="https://identity.nexar.com/connect/token",
            oauth_scopes="openid design.domain user.access",
        )
