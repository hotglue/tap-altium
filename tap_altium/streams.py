"""Stream type classes for tap-altium."""

from __future__ import annotations

from pathlib import Path
from typing import Iterable, Optional, Any
import requests

from singer_sdk import typing as th

from tap_altium.client import altiumStream

SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")

class WorkspacesStream(altiumStream):
    name = "workspace"

    # schema_filepath = SCHEMAS_DIR / "users.json"  # noqa: ERA001
    schema_filepath = SCHEMAS_DIR / "workspaces_schema.json"
    primary_keys = ["id"]
    replication_key = None
    records_jsonpath = "$.data.desWorkspaces[*]"


    def query(self,context = {}):
      return """
        query workspaces {
          desWorkspaces {
              url
          }
      }
      """
    def get_child_context(self, record: dict, context: dict | None) -> dict | None:
        return {
            "workspaceUrl":record["url"]
        }


class BOMStream(altiumStream):
    name = "boms"

    # schema_filepath = SCHEMAS_DIR / "users.json"  # noqa: ERA001
    schema_filepath = SCHEMAS_DIR / "boms_schema.json"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = WorkspacesStream
    records_jsonpath = "$.data.desProjects.nodes[*]"

    def query(self,context):
        return f"""
          query projects {{
            desProjects(workspaceUrl: \"{context["workspaceUrl"]}\") {{
                nodes {{
                    name
                    id
                    design {{
                        variants {{
                            bom {{
                                id
                            }}
                        }}
                        releases {{
                            nodes {{
                                id
                                description
                                variants {{
                                    name
                                    bom {{
                                        id
                                    }}
                                }}
                            }}
                        }}
                    }}
                }}
              }}
        }}
      """
    def get_child_context(self, record: dict, context: dict | None) -> dict | None:
        child_context = {"bomProjectId": record.get("id")}
        if record["design"]["variants"]:
            bomIds = list(map(lambda x: x["bom"]["id"], list(record["design"]["variants"])))
            bomIds = list(set(bomIds))
            child_context["bomIds"] = bomIds
        return child_context

    def _sync_children(self, child_context: dict) -> None:
        for child_stream in self.child_streams:
            if child_stream.selected or child_stream.has_selected_descendents:
                if child_context.get("bomIds"):
                    child_stream.sync(context=child_context)


class BOMItemsStream(altiumStream):
    name = "bom_items"
    parent_stream_type = BOMStream
    schema_filepath = SCHEMAS_DIR / "bomitems_schema.json"
    replication_key = None
    records_jsonpath = "$.data.desBomById.bomItems[*]"
    bom_index = 0
    bom_ids = None
    def query(self,context):
      if not self.bom_ids:
          self.bom_ids = context.get("bomIds")
      return f"""
            query bomItems {{
              desBomById(id: \"{self.bom_ids[self.bom_index]}\") {{
                  bomItems {{
                      component {{
                          id
                          comment
                          description
                          manufacturerParts{{
                            partNumber
                            companyName
                          }}
                      }}
                      bomItemInstances {{
                          designator
                          isFitted
                      }}
                      quantity

                  }}
                }}
          }}
      """

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        bom_ids = self.bom_ids
        ids_len = len(bom_ids)
        previous_token = previous_token or 0
        if ids_len > 0 and previous_token < ids_len - 1:
            next_page_token = previous_token + 1
            self.bom_index = next_page_token
            return next_page_token
        else:
            return None

